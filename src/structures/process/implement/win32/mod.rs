/// for external process usage
pub mod external;
/// for internal process usage
pub mod internal;